
var turno="X";
var nTurnos=0;
var fin=false;

//Punto de entrada al programa
function iniciarJuego(){
    //Crear 9 botones
    let miBoton;
    let formulario;
    let i;
    let salto;

    //Crear un objeto que represente al formuario
    formulario=document.getElementById("formTablero");

    for(i=1;i<10;i++){
        //Crear un objeto de tipo input
        miBoton=document.createElement("input");
    
        //Agregar atributos al objeto
        miBoton.setAttribute("type","button");
        miBoton.setAttribute("id","boton"+i);
        miBoton.setAttribute("class","boton");
        miBoton.setAttribute("value"," ");
        miBoton.setAttribute("onclick","realizarJugada(this)");  
        formulario.appendChild(miBoton);

        if(i%3==0){
            salto=document.createElement("br");
            formulario.appendChild(salto);
        }
    }
    
}

function realizarJugada(elemento){

    if (elemento.value === "X" || elemento.value === "O") {
        alert("Esta casilla esta ocupada");
      }
    else{
        elemento.value=turno;
        nTurnos++;
        if(turno==="X"){
            turno="O";
        }else{
            turno="X";
        }
    }
    evaluarTriqui();
    if (nTurnos==9 && fin==false){
        alert("Fin del juego \n Empate")
        fin=true;
    }
    reiniciar();
    
}
function evaluarTriqui(){
    let i;
    let casilla1;
    let casilla2;
    let casilla3;

    //Evaluar en filas
    for (i = 0; i < 9; i = i + 3) {
        casilla1 = document.getElementById("boton" + (i + 1)).value;
        casilla2 = document.getElementById("boton" + (i + 2)).value;
        casilla3 = document.getElementById("boton" + (i + 3)).value;
        if (casilla1 === casilla2 && casilla1 === casilla3 && casilla1 != " ") {
            alert("Fin del juego \n Ganador "+casilla1);
            fin=true;
        }
    }
    //Evaluar en columnas
    for (i = 1; i < 4; i++) {
        casilla1 = document.getElementById("boton" + i).value;
        casilla2 = document.getElementById("boton" + (i + 3)).value;
        casilla3 = document.getElementById("boton" + (i + 6)).value;
        if (casilla1 === casilla2 && casilla1 === casilla3 && casilla1 != " ") {
            alert("Fin del juego \n Ganador "+casilla1);
            fin=true;
        }
    }

  //Evaluar en diagonales

  for (i = 2; i < 5; i=i+2) {
    casilla1 = document.getElementById("boton" + 5).value;
    casilla2 = document.getElementById("boton" + (5 + i)).value;
    casilla3 = document.getElementById("boton" + (5 - i)).value;
    if (casilla1 === casilla2 && casilla1 === casilla3 && casilla1 != " ") {
        alert("Fin del juego \n Ganador "+casilla1);
        fin=true;
    }
  }


}

function reiniciar() {
    if (fin==true){
        nTurnos = 0;
        turno = "X";
        for (i = 1; i < 10; i++) {
            elemento = document.getElementById("boton" + i);
            elemento.value = " ";
            fin=false;

       }
    }
}   